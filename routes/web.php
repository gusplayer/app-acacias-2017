<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ViewsController@inicio');
Route::get('/login', 'ViewsController@login');
Route::get('/registrar', 'ViewsController@registrar');
Route::post('/registrar', 'ViewsController@register');
Route::get('/home', 'ViewsController@home');
Route::post('/agregar', 'EstablecimientoController@agregar');

Auth::routes();

Route::post('/mi-establecimiento/eliminar/eventos', 'EventosMiEstablecimientoController@destroy');
Route::resource('/mi-establecimiento/eventos', 'EventosMiEstablecimientoController');
Route::resource('/mi-establecimiento', 'MiEstablecimientoController');
Route::resource('/establecimiento/promociones', 'PromocionesMiEstablecimientoController');
Route::post('/eliminar/promocion/establecimiento', 'PromocionesMiEstablecimientoController@destroy');

Route::get('/inicio', 'HomeController@index')->name('inicio');

Route::post('/eliminar/noticia', 'NoticiasController@destroy');
Route::resource('/noticias', 'NoticiasController');

Route::get('/establecimientos/cambiar/{id}/{estado}', 'EstablecimientosController@cambiarEstado');
Route::resource('/establecimientos', 'EstablecimientosController');

Route::get('/lugares/cambiar/{id}/{estado}', 'LugaresController@cambiarEstado');
Route::resource('/lugares', 'LugaresController');

Route::get('/eventos/cambiar/{id}/{estado}', 'EventosController@cambiarEstado');
Route::resource('/eventos', 'EventosController');
Route::post('/eliminar/evento', 'EventosController@destroy');

Route::resource('/categoria-lugares', 'CategoriasLugaresController');
Route::resource('/categoria-eventos', 'CategoriaEventosController');
Route::resource('/categoria-noticias', 'CategoriaNoticiasController');
Route::resource('/categoria-promociones', 'CategoriaPromocionesController');


Route::resource('/subcategoria-lugares', 'SubCategoriasLugaresController');
Route::resource('/subcategoria-eventos', 'SubCategoriasEventosController');
Route::resource('/subcategoria-noticias', 'SubCategoriasNoticiasController');
Route::resource('/subcategoria-promociones', 'SubCategoriasPromocionesController');