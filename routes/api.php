<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/noticias', 'Api\NoticiasController');
Route::resource('/noticias/categoria', 'Api\CategoriasNoticiasController');

Route::resource('/lugares', 'Api\LugaresController');
Route::resource('/lugares/categoria', 'Api\CategoriasLugaresController');
Route::resource('/lugares/subcategoria', 'Api\SubcategoriasLugaresController');

Route::resource('/eventos', 'Api\EventosController');
Route::resource('/eventos/categoria', 'Api\CategoriasEventosController');

Route::resource('/promociones', 'Api\PromocionesController');
Route::resource('/promociones/categoria', 'Api\CategoriasPromocionesController');

Route::resource('/search', 'Api\SearchController');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
