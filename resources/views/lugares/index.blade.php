@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Lugares creados</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <a href="/lugares/create" type="button" class="btn btn-primary btn-sm">
                                            Crear nuevo lugar
                                        </a>

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Lugares creados</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="myTable" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th>Categoria</th>
                                                        <th>Descripción</th>
                                                        <th>Fecha creación</th>
                                                        <th>Fecha actualización</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($establecimientos)
                                                        @foreach ($establecimientos as $establecmiento)
                                                            <tr>
                                                                <td>{!! $establecmiento->id !!}</td>
                                                                <td>{!! $establecmiento->titulo !!}</td>
                                                                <td>{!! $establecmiento->categoria->nombre !!}</td>
                                                                <td>{!! html_entity_decode(substr($establecmiento->descripcion, 0, 30))!!}
                                                                    ...
                                                                </td>
                                                                <td>{!! $establecmiento->created_at !!}</td>
                                                                <td>{!! $establecmiento->updated_at !!}</td>
                                                                <td>

                                                                    <a href="/lugares/{!! $establecmiento->id !!}/edit"
                                                                       class="btn btn-info">Editar</a>

                                                                    @if($establecmiento->estado == 0 )
                                                                        <a href="/lugares/{!! $establecmiento->id !!}/activar"
                                                                           class="btn btn-success">Activar</a>
                                                                        @else
                                                                        <a href="/lugares/{!! $establecmiento->id !!}/desactivar"
                                                                           class="btn btn-warning">Desactivar</a>
                                                                    @endif
                                                                <!--
                                                                    <a style="text-decoration: none;"
                                                                       data-toggle="modal" href="#deleteModal"
                                                                       class="push1 btn btn-danger"
                                                                       id="{! $establecmiento->id !!}">Eliminar</a>
                                                                       -->
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th>Categoria</th>
                                                        <th>Descripción</th>
                                                        <th>Fecha creación</th>
                                                        <th>Fecha actualización</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>


                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Eliminar Lugar</h4>
                                            </div>
                                            <form action="/eliminar/noticia" method="POST"
                                                  enctype="multipart/form-data">

                                                {!! csrf_field() !!}
                                                <div class="modal-body">
                                                    <p>Esta seguro que desea eliminar este lugar ?</p>

                                                    <input id="id-banners" type="hidden" name="id" required
                                                           class="form form-control">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        Cerrar
                                                    </button>
                                                    <input type="submit" class="btn btn-success" value="Eliminar">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $('.push1').click(function () {
                var essay_id = $(this).attr('id');
                $('#id-banners').val(essay_id);

                <?php
                $var_example_php = "<script>document.write(essay_id);</script>";
                ?>
            });
        });
    </script>

@endsection
