@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">{!! $establecimiento->titulo !!}</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        <label>Imágen lugar</label>

                        <br>
                        <div class="col-md-6">
                            <img src="/img_lugar/{!! $establecimiento->imagen_principal !!}">
                        </div>

                        <form action="/lugares/{!! $establecimiento->id !!}" method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <input type="hidden" name="_method" value="PUT">

                            <div class="panel-body">

                                <div class="col-md-12">

                                    <div class="col-md-6">
                                        <label>Nombre (*)</label>
                                        <input class="form form-control" value="{!! $establecimiento->titulo !!}" type="text" required name="titulo">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Imágen </label>
                                        <input class="form form-control" type="file" name="imagen">
                                        <p>Si no agrega una imágen, la imágen de arriba no será editada.</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Categoria (*)</label>
                                        <select name="categoria" class="form form-control" required>
                                            {!! $establecimiento->categoria_lugares_id !!}
                                            @foreach($categorias as $categoria)
                                                @if($categoria->id == $establecimiento->categoria_lugares_id)
                                                    <option value="{!! $categoria->id !!}"
                                                            selected>{!! $categoria->nombre !!}</option>
                                                @else
                                                    <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                                @endif
                                            @endforeach()
                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        <p style="color: red;">Los campos con (*) son campos requeridos</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Teléfono</label>
                                        <input class="form form-control" type="text" value="{!! $establecimiento->telefono !!}" name="telefono">
                                    </div>

                                    <div class="col-md-12">
                                        <label>Descripcion</label>
                                        <textarea class="form form-control" name="descripcion">{!! $establecimiento->descripcion !!}</textarea>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Página Web</label>
                                        <input class="form form-control" value="{!! $establecimiento->web !!}" type="text" name="web">
                                    </div>
                                    <br>

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label>Latitud</label>
                                            <input id="lat" class="form form-control" value="{!! $establecimiento->latitud !!}" type="text"
                                                   name="lat">
                                        </div>

                                        <div class="col-md-6">
                                            <label>Longitud</label>
                                            <input id="lng" class="form form-control" value="{!! $establecimiento->longitud !!}" type="text"
                                                   name="lng">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                        <div class="product-box">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="product-title">
                                                        <b>Dirección</b>
                                                        <input name="direccion" value="{!! $establecimiento->direccion !!}" id="pac-input"
                                                               class="form-control"
                                                               type="text"
                                                               placeholder="Ingrese la dirección">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <p style="color: red;">Arrastre el marcador por el mapa, para editar la ubicación</p>

                                        <div class="row">
                                            <div id="map" class="col-md-12 clearfix"
                                                 style="width: -webkit-fill-available; height: 400px;"></div>
                                        </div>
                                    </div> <!-- /product-box -->

                                    <div class="col-md-6" style="margin-top: 20px;">
                                        <button type="submit" class="btn btn-success">Editar</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var latitude = '{!! $establecimiento->latitud !!}';
        var longitude = '{!! $establecimiento->longitud !!}';

        function initMap() {
            var myLatlng = new google.maps.LatLng(latitude, longitude);
            var mapOptions = {
                zoom: 15,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: true,
                title: name
            });

            google.maps.event.trigger(map, "resize");

            google.maps.event.addListener(marker, 'dragend', function (event) {
                $("#lat").val(this.getPosition().lat());
                $("#lng").val(this.getPosition().lng());
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAntS5-z29ATmHs8xPYbkULe3lyPVkHzok&callback=initMap">
    </script>

    <script>
        CKEDITOR.replace('descripcion');
    </script>

@endsection