@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Crear subcategoria Noticias</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">
                        <form action="/subcategoria-noticias" method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="col-md-8">
                                <label>Nombre</label>
                                <input id="nombre" name="nombre" value="{!! old('nombre') !!}" type="text"
                                       class="form form-control" required>
                            </div>

                            <div class="col-md-8">
                                <label>Categoria principal noticias</label>
                                <select required class="form form-control" name="categoria">
                                    @foreach($categorias as $categoria)
                                        <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Guardar subcategoria">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        CKEDITOR.replace( 'descripcion' );
    </script>

@endsection