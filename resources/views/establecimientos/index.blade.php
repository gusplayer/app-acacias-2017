@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Establecimientos creados</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <a href="/establecimientos/create" type="button" class="btn btn-primary btn-sm">
                                            Crear nuevo establecimiento
                                        </a>

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Establecimientos creados</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">

                                                <table id="myTable" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <td>Id</td>
                                                        <td>Nombre</td>
                                                        <td>Descripción</td>
                                                        <td>Fecha creación</td>
                                                        <td>Fecha actualización</td>
                                                        <td>Editar</td>
                                                        <td>Activar/Desactivar</td>
                                                        <td>Eliminar</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($establecimientos)
                                                        @foreach ($establecimientos as $establecmiento)
                                                            <tr>
                                                                <td>{!! $establecmiento->id !!}</td>
                                                                <td>{!! $establecmiento->titulo !!}</td>
                                                                <td>{!! html_entity_decode(substr($establecmiento->descripcion, 0, 30))!!}
                                                                    ...
                                                                </td>
                                                                <td>{!! $establecmiento->created_at !!}</td>
                                                                <td>{!! $establecmiento->updated_at !!}</td>
                                                                <td>

                                                                    <a href="/establecimientos/{!! $establecmiento->id !!}/edit"
                                                                       class="btn btn-info">
                                                                        <i class="fa fa-pencil"></i> Editar</a>
                                                                <td>
                                                                    @if($establecmiento->estado == 0 )
                                                                        <a href="/establecimientos/cambiar/{!! $establecmiento->id !!}/activar"
                                                                           class="btn btn-success">
                                                                            <i class="fa fa-check-circle-o"></i> Activar</a>
                                                                    @else
                                                                        <a href="/establecimientos/cambiar/{!! $establecmiento->id !!}/desactivar"
                                                                           class="btn btn-warning">
                                                                            <i class="fa fa-warning"></i> Desactivar</a>
                                                                    @endif
                                                                </td>

                                                                <td>
                                                                    <a style="text-decoration: none;"
                                                                       data-toggle="modal" href="#deleteModal"
                                                                       class="push1 btn btn-danger"
                                                                       id="{!! $establecmiento->id !!}">Eliminar</a>

                                                                </td>

                                                                <!--
                                                                <a style="text-decoration: none;"
                                                                   data-toggle="modal" href="#deleteModal"
                                                                   class="push1 btn btn-danger"
                                                                   id="{! $establecmiento->id !!}">Eliminar</a>
                                                                   -->
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td>Id</td>
                                                        <td>Nombre</td>
                                                        <td>Descripción</td>
                                                        <td>Fecha creación</td>
                                                        <td>Fecha actualización</td>
                                                        <td>Editar</td>
                                                        <td>Activar/Desactivar</td>
                                                        <td>Eliminar</td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>


                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <form method="POST" id="url_eliminar_establecimiento">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="_method" value="DELETE" />

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Eliminar
                                                        Establecimiento</h4>
                                                </div>
                                                <input type="hidden" name="_method" value="DELETE">
                                                {!! csrf_field() !!}
                                                <div class="modal-body">
                                                    <p>Esta seguro que desea eliminar este establecimiento ?</p>

                                                    <input id="id-establecimiento" type="hidden" name="id" required
                                                           class="form form-control">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        Cerrar
                                                    </button>
                                                    <input type="submit" class="btn btn-success" value="Eliminar">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>

    <script>
        $('#establecimientos').DataTable({});
    </script>

    <script>
        $(document).ready(function () {
            $('.push1').click(function () {
                var essay_id = $(this).attr('id');
                $('#id-establecimiento').val(essay_id);

                $("#url_eliminar_establecimiento").attr("action", '/establecimientos/'+essay_id);

                <?php
                $var_example_php = "<script>document.write(essay_id);</script>";
                ?>
            });
        });
    </script>

@endsection
