@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-error">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="/agregar" method="post" enctype="multipart/form-data">

                        {!! csrf_field() !!}

                        <div class="panel-body">

                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label>Nombre propietario</label>
                                    <input class="form form-control" type="text" required name="name">
                                </div>

                                <div class="col-md-6">
                                    <label>Correo</label>
                                    <input class="form form-control" type="email" value="{!! $email !!}" required name="email">
                                </div>

                                <div class="col-md-6">
                                    <label>Contraseña</label>
                                    <input class="form form-control" type="password" required name="password">
                                </div>
                            </div>

                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <label>Nombre establecimiento</label>
                                    <input class="form form-control" type="text" required name="titulo">
                                </div>

                                <div class="col-md-6">
                                    <label>Imágen</label>
                                    <input class="form form-control" type="file" required name="imagen">
                                </div>

                                <div class="col-md-6">
                                    <label>Nit/Cédula</label>
                                    <input class="form form-control" type="text" required name="nit">
                                </div>

                                <div class="col-md-6">
                                    <label>Dirección</label>
                                    <input class="form form-control" type="text" required name="direccion">
                                </div>

                                <div class="col-md-6">
                                    <label>Teléfono</label>
                                    <input class="form form-control" type="text" name="telefono">
                                </div>

                                <div class="col-md-12">
                                    <label>Descripcion</label>
                                    <textarea class="form form-control" name="descripcion"></textarea>
                                </div>

                                <div class="col-md-6">
                                    <label>Página Web</label>
                                    <input class="form form-control" type="text" name="web">
                                </div>

                                <div class="col-md-6" style="margin-top: 20px;">
                                    <button type="submit" class="btn btn-success"> Agregar mi negocio</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var options = {
            enableHighAccuracy: true,
            timeout: 6000,
            maximumAge: 0
        };

        navigator.geolocation.getCurrentPosition(success, error, options);

        function success(position) {
            var coordenadas = position.coords;

            console.log('Tu posición actual es:');
            console.log('Latitud : ' + coordenadas.latitude);
            console.log('Longitud: ' + coordenadas.longitude);
            console.log('Más o menos ' + coordenadas.accuracy + ' metros.');
        };

        function error(error) {
            console.warn('ERROR(' + error.code + '): ' + error.message);
        };
    </script>
@endsection
