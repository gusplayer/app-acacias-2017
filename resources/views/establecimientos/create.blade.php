@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        <form action="/establecimientos" method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="panel-body">

                                <div class="col-md-12">
                                    <div class="col-md-8">
                                        <label>Nombre propietario</label>
                                        <input class="form form-control" type="text" name="name">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Correo propietario</label>
                                        <input class="form form-control" type="email" name="email">
                                        <p>Email para ingreso al sistema (propietario)</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Contraseña</label>
                                        <input class="form form-control" type="password" name="password">
                                        <p>Contraseña para ingreso al sistema (propietario)</p>
                                    </div>

                                    <div class="col-md-12">

                                        <p style="color: red;">Si solo se quiere agregar un lugar omita los datos de
                                            arriba...</p>
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="col-md-6">
                                        <label>Nombre establecimiento (*)</label>
                                        <input class="form form-control" value="{!! old('titulo') !!}" type="text" required name="titulo">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Imágen (*)</label>
                                        <input class="form form-control" type="file" required name="imagen">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Categoria (*)</label>
                                        <select name="categoria" class="form form-control" required>
                                            @foreach($categorias as $categoria)
                                                <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                            @endforeach()
                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        <p style="color: red;">Los campos con (*) son campos requeridos</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Nit/Cédula</label>
                                        <input class="form form-control" type="text" value="{!! old('nit') !!}" required name="nit">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Teléfono</label>
                                        <input class="form form-control" type="text" value="{!! old('telefono') !!}" name="telefono">
                                    </div>

                                    <div class="col-md-12">
                                        <label>Descripcion</label>
                                        <textarea class="form form-control" value="{!! old('descripcion') !!}" name="descripcion"></textarea>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Página Web</label>
                                        <input class="form form-control" value="{!! old('web') !!}" type="text" name="web">
                                    </div>

                                    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                        <div class="product-box">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="product-title">
                                                        <b>Ubicación</b>
                                                        <input name="direccion" id="pac-input" class="form-control"
                                                               type="text"
                                                               placeholder="Ingrese la dirección">
                                                    </div>
                                                </div>
                                            </div>
                                            <input id="lat" type="hidden" name="lat"/>
                                            <input id="lng" type="hidden" name="lng"/>
                                        </div>

                                        <div class="row">
                                            <div id="map" class="col-md-12 clearfix"
                                                 style="width: -webkit-fill-available; height: 400px;"></div>
                                        </div>
                                    </div> <!-- /product-box -->

                                    <div class="col-md-6" style="margin-top: 20px;">
                                        <button type="submit" class="btn btn-success"> Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 3.9900573, lng: -73.7721802},
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // [START region_getplaces]
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(50, 50),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(8, 18),
                        scaledSize: new google.maps.Size(10, 10)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location,
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    document.getElementById('lat').value = place.geometry.location.lat();
                    document.getElementById('lng').value = place.geometry.location.lng();
                });
                map.fitBounds(bounds);
            });
        }


    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgDMPERKh3jkW_rEiW1X-IyvGGB6E-LUU&libraries=places&callback=initAutocomplete"
            async defer></script>

    <script>
        CKEDITOR.replace('descripcion');
    </script>

@endsection