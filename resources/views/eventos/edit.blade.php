@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        <label>Imágen noticia</label>
                        <br>
                        <div class="col-md-6 col-md-offset-3">
                            <img class="img img-responsive" src="/img_eventos/{!! $evento->imagen_principal !!}">
                        </div>
                        <form action="/eventos/{!! $evento->id !!}" method="POST" enctype="multipart/form-data">

                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="col-md-8">
                                <label>Evento</label>
                                <input id="titulo" name="titulo" value="{!! $evento->titulo !!}" type="text"
                                       class="form form-control" required>
                            </div>

                            <div class="col-md-12">
                                <label>Descripción</label>
                                <textarea name="descripcion">{!! $evento->descripcion !!}</textarea>
                            </div>

                            <div class="col-md-8">
                                <label>Imágen</label>
                                <input type="file" name="imagen" class="form form-control">
                            </div>

                            <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                <div class="product-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="product-title">
                                                <b>Lugar</b>
                                                <input name="lugar" value="{!! $evento->lugar !!}" id="pac-input"
                                                       class="form-control"
                                                       type="text"
                                                       placeholder="Ingrese el lugar del evento">
                                            </div>
                                        </div>
                                    </div>
                                    <input id="lat" type="hidden" name="lat" value="{!! $evento->latitud !!}"/>
                                    <input id="lng" type="hidden" name="lng" value="{!! $evento->longitud !!}"/>
                                </div>

                                <p style="color: red;">Arrastre el marcador por el mapa, para editar la ubicación</p>

                                <div class="row">
                                    <div id="map" class="col-md-12 clearfix"
                                         style="width: -webkit-fill-available; height: 400px;"></div>
                                </div>
                            </div> <!-- /product-box -->

                            <div class="col-md-8">
                                <label>Categoria</label>
                                <select name="categoria" class="form form-control" required>
                                    @foreach($categories as $categoria)
                                        @if($categoria->id == $evento->categoria_eventos_id)
                                            <option value="{!! $categoria->id !!}"
                                                    selected>{!! $categoria->nombre !!}</option>
                                        @else
                                            <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                        @endif
                                    @endforeach()
                                </select>
                            </div>

                            <div class="col-md-8">
                                <label>Url Youtube</label>
                                <input type="url" name="video" value="{!! $evento->video!!}"
                                       class="form form-control">
                            </div>

                            <br>
                            <br>
                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Editar evento">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        var latitude = '{!! $evento->latitud !!}';
        var longitude = '{!! $evento->longitud !!}';

        function initMap() {
            var myLatlng = new google.maps.LatLng(latitude, longitude);
            var mapOptions = {
                zoom: 15,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: true,
                title: name
            });

            google.maps.event.trigger(map, "resize");

            google.maps.event.addListener(marker, 'dragend', function (event) {
                $("#lat").val(this.getPosition().lat());
                $("#lng").val(this.getPosition().lng());
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAntS5-z29ATmHs8xPYbkULe3lyPVkHzok&callback=initMap">
    </script>

    <script>
        CKEDITOR.replace('descripcion');
    </script>


@endsection