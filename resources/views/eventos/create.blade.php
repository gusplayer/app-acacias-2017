@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">
                        <form action="/eventos" method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="col-md-8">
                                <p style="color: red;">Campos obligatorios (*)</p>
                            </div>

                            <div class="col-md-8">
                                <label>Titulo (*)</label>
                                <input id="titulo" name="titulo" value="{!! old('titulo') !!}" type="text"
                                       class="form form-control" required>
                            </div>

                            <div class="col-md-12">
                                <label>Descripción</label>
                                <textarea name="descripcion">{!! old('descripcion')  !!}</textarea>
                            </div>

                            <div class="col-md-8">
                                <label>Imágen (*)</label>
                                <input type="file" name="imagen" class="form form-control" required>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
                                <div class="product-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="product-title">
                                                <b>Ubicación del evento</b>
                                                <input name="lugar" id="pac-input" class="form-control"
                                                       type="text"
                                                       placeholder="Ingrese el lugar del evento">
                                            </div>
                                        </div>
                                    </div>
                                    <input id="lat" type="hidden" name="lat"/>
                                    <input id="lng" type="hidden" name="lng"/>
                                </div>

                                <div class="row">
                                    <div id="map" class="col-md-12 clearfix"
                                         style="width: -webkit-fill-available; height: 400px;"></div>
                                </div>
                            </div> <!-- /product-box -->

                            <div class="col-md-8">
                                <label>Categoria (*)</label>
                                <select name="categoria" class="form form-control" required>
                                    @foreach($categories as $categoria)
                                        <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                    @endforeach()
                                </select>
                                <p>No encuentras la categoria ? <a href="/categorias">Crear categoria</a></p>
                            </div>

                            <div class="col-md-8">
                                <label>Url Youtube</label>
                                <input type="url" name="video" value="{!! old('video') !!}"
                                       class="form form-control">
                            </div>

                            <br>
                            <br>
                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Guardar evento">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 3.9900573, lng: -73.7721802},
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // [START region_getplaces]
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(50, 50),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(8, 18),
                        scaledSize: new google.maps.Size(10, 10)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location,
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    document.getElementById('lat').value = place.geometry.location.lat();
                    document.getElementById('lng').value = place.geometry.location.lng();
                });
                map.fitBounds(bounds);
            });
        }


    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgDMPERKh3jkW_rEiW1X-IyvGGB6E-LUU&libraries=places&callback=initAutocomplete"
            async defer></script>

    <script>
        CKEDITOR.replace('descripcion');
    </script>

@endsection