@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        <form action="/categoria-promociones/{!! $categoria->id !!}" method="POST" enctype="multipart/form-data">

                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="col-md-8">
                                <label>Nombre</label>
                                <input id="nombre" name="nombre" value="{!! $categoria->nombre !!}" type="text"
                                       class="form form-control" required>
                            </div>


                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Editar categoria">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection