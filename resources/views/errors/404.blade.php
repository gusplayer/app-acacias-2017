<!DOCTYPE html>
<html lang="es-CO">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>Error 404</title>
    <!-- Favicon -->
    <link rel="icon" href="/img/acacias_pqtq.ico">
    <!-- Para que te quedes CSS -->
    <link href="/css/404.css" rel="stylesheet">
    <!-- Core Stylesheet -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="/css/responsive.css" rel="stylesheet">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-93697026-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<!-- ***** 404 page ***** -->
<section class="page_404">
    <div>
        <h1>
            ¡Ops!<br/>
            ¡Se ha producido algún error!
        </h1>
        <img src="/img/pvb-broken.png">
        <h4>
            Vuelve a la página de
            <a href="http://pqtq.acacias.gov.co">inicio</a>.
        </h4>
    </div>
</section>
<!-- ***** Wellcome Area End ***** -->
<div class="fix_bg"></div>
<!-- ***** Footer Area Start ***** -->
<!-- Jquery-2.2.4 JS -->
<script src="/js/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="/js/popper.min.js"></script>
<!-- Bootstrap-4 Beta JS -->
<script src="/js/bootstrap.min.js"></script>
<!-- All Plugins JS -->
<script src="/js/plugins.js"></script>
<!-- Slick Slider Js-->
<script src="/js/slick.min.js"></script>
<!-- Active JS -->
<script src="/js/active.js"></script>
</body>
</html>
