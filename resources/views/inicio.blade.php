 <!DOCTYPE html>
<html lang="es-CO">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title>Acacías... para que te quedes!</title>
    <!-- Favicon -->
    <link rel="icon" href="/img/core-img/acacias_pqtq.ico">
    <!-- Para que te quedes CSS -->
    <link href="/css/pqtq.css" rel="stylesheet">
    <!-- Core Stylesheet -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="/css/responsive.css" rel="stylesheet">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-93697026-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<!-- Preloader Start -->
<div id="preloader">
    <div class="colorlib-load"></div>
</div>
<!-- ***** Header Area Start ***** -->
<header class="header_area animated">
    <div class="container-fluid">
        <div class="row align-items-center">
            <!-- Menu Area Start -->
            <div class="col-12 col-lg-10">
                <div class="menu_area">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- Logo -->
                        <a class="navbar-brand" href="#">Bienvenidos</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Menu Area -->
                        <div class="collapse navbar-collapse" id="ca-navbar">
                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item active"><a class="nav-link" href="#inicio">Inicio</a></li>
                                <li class="nav-item"><a class="nav-link" href="#descubre">Descubre</a></li>
                                <li class="nav-item"><a class="nav-link" href="#descarga">Descarga</a></li>
                                <li class="nav-item"><a class="nav-link" href="#comentarios">Comentarios</a></li>
                                <li class="nav-item"><a class="nav-link" href="#contacto">Contactenos</a></li>
                            </ul>
                            <div class="sing-up-button d-lg-none">
                                <a href="/login">Iniciar Sesión</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Signup btn -->
            <div class="col-12 col-lg-2">
                <div class="sing-up-button d-none d-lg-block">
                    <a href="/login">Iniciar sesión</a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->
<!-- ***** Wellcome Area Start ***** -->
<section class="wellcome_area clearfix" id="inicio">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 col-md">
                <div class="wellcome-heading">
                    <h2>Acacías... <i>para que te quedes!</i></h2>
                    <h3>Acacías</h3>
                    <p>
                        La aplicación móvil para el municipio de Acacías.<br>
                        !Descubre todo lo que nuestro municipio tiene para tí!
                    </p>
                </div>
                <div class="get-start-area">
                    <!-- Form Start -->
                    <form action="/registrar" method="post" class="form-inline">
                        {!! csrf_field() !!}
                        <input type="email" name="email" required class="form-control email" placeholder="correo@negocio.com">
                        <input type="submit" class="submit" value="Registra tu negocio">
                    </form>
                    <!-- Form End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Welcome thumb -->
    <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
        <img class="mod_img" src="/img/bg-img/movil_app.png" alt="">
    </div>
</section>
<!-- ***** Wellcome Area End ***** -->
<div class="fix_bg"></div>
<!-- ***** Special Area Start ***** -->
<section class="special-area bg-white section_padding_50_0" id="descubre">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading Area -->
                <div class="section-heading text-center">
                    <h2>¡Descubre la app!</h2>
                    <div class="line-shape"></div>
                    <p>
                        <b>Acacías... <i>para que te quedes! </i></b>es la nueva aplicación móvil del
                        municipio de Acacías, en ella encontrarás toda la información importante de nuestro
                        municipio.<br/>
                        Encontrarás toda la información sobre lugares, negocios, eventos, promociones de
                        los negocios locales y mucho más.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <i class="ti-location-pin" aria-hidden="true"></i>
                    </div>
                    <h4>Lugares</h4>
                    <p>
                        Encuentra el lugar que estes buscando. Negocios, colegios, parques, bancos,
                        sitios turísticos y entidades públicas.
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-icon">
                        <i class="ti-align-left" aria-hidden="true"></i>
                    </div>
                    <h4>Noticias</h4>
                    <p>
                        Lee las últimas noticias publicadas por la alcaldía del municipio de Acacías.
                        <br/><br/>
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                    <div class="single-icon">
                        <i class="ti-calendar" aria-hidden="true"></i>
                    </div>
                    <h4>Eventos</h4>
                    <p>
                        Todos los eventos que se realicen en el municipio los puedes encontrar aquí.
                        <br/><br/>
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                    <div class="single-icon">
                        <i class="ti-shopping-cart" aria-hidden="true"></i>
                    </div>
                    <h4>Promociones</h4>
                    <p>
                        Aprovecha las promociones que los diferentes negocios de Acacías ofrecen.
                        <br/><br/>
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                    <div class="single-icon">
                        <i class="ti-user" aria-hidden="true"></i>
                    </div>
                    <h4>Registrate</h4>
                    <p>
                        Registrate en nuestra plataforma para publicar tu negocio, eventos y promociones
                        en la app.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Special Area End ***** -->
<!-- ***** Sección de descargar la app ***** -->
<section id="descarga">
    <!-- Special Description Area -->
    <div class="special_description_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="special_description_img">
                        <img src="/img/bg-img/app.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-xl-5 ml-xl-auto">
                    <div class="special_description_content">
                        <h2>Descarga nuestra aplicación!</h2>
                        <p>
                            Nuestra aplicación "<b>Acacías... <i>para que te quedes</i></b>" se encuentra
                            disponible para dispositivos móviles Android.
                        </p>
                        <div class="app-download-area">
                            <div class="app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                <!-- Google Store Btn -->
                                <a href="https://play.google.com/store/apps/details?id=acacias.lacharme.initics.acacias&hl=es">
                                    <i class="fa fa-android"></i>
                                    <p class="mb-0"><span>disponible en </span>Google Play Store</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Fin de la sección de descargar la app ***** -->
<!-- ***** Awesome Features Start ***** -->
<section class="awesome-feature-area bg-white section_padding_50 clearfix" id="features">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Heading Text -->
                <div class="section-heading text-center">
                    <h2>¡Mírala en acción!</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Awesome Features End ***** -->
<!-- ***** Video Area Start ***** -->
<div class="video-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Video Area Start -->
                <div class="video-area">
                    <div class="video-play-btn">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/N1xFicJf2eE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Video Area End ***** -->
<!-- ***** Cool Facts Area Start ***** -->
<section class="cool_facts_area clearfix">
    <div class="container">
        <div class="row">
            <!-- Single Cool Fact-->
            <div class="col-12 col-md-3 col-lg-3">
                <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="counter-area">
                        <h3><span class="counter">9</span></h3>
                    </div>
                    <div class="cool-facts-content">
                        <i class="ion-arrow-down-a"></i>
                        <p>Descargas <br>de la App</p>
                    </div>
                </div>
            </div>
            <!-- Single Cool Fact-->
            <div class="col-12 col-md-3 col-lg-3">
                <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="counter-area">
                        <h3><span class="counter">+{!! $cantidad_negocios !!}</span></h3>
                    </div>
                    <div class="cool-facts-content">
                        <i class="ion-ios-location-outline"></i>
                        <p>Lugares <br>registrados</p>
                    </div>
                </div>
            </div>
            <!-- Single Cool Fact-->
            <div class="col-12 col-md-3 col-lg-3">
                <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.6s">
                    <div class="counter-area">
                        <h3><span class="counter">{!! $cantidad_promociones !!}</span></h3>
                    </div>
                    <div class="cool-facts-content">
                        <i class="ion-ios-pricetag"></i>
                        <p>Promociones <br>activas</p>
                    </div>
                </div>
            </div>
            <!-- Single Cool Fact-->
            <div class="col-12 col-md-3 col-lg-3">
                <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.8s">
                    <div class="counter-area">
                        <h3><span class="counter">9</span></h3>
                    </div>
                    <div class="cool-facts-content">
                        <i class="ion-ios-star-outline"></i>
                        <p>Calificaciones <br>en total</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Cool Facts Area End ***** -->
<!-- ***** App Screenshots Area Start ***** -->
<section class="app-screenshots-area bg-white section_padding_50 clearfix" id="screenshot">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <!-- Heading Text  -->
                <div class="section-heading">
                    <h2>Así se ve la App</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- App Screenshots Slides  -->
                <div class="app_screenshots_slides owl-carousel">
                    <div class="single-shot">
                        <img src="/img/scr-img/app-1.png" alt="">
                    </div>
                    <div class="single-shot">
                        <img src="/img/scr-img/app-2.png" alt="">
                    </div>
                    <div class="single-shot">
                        <img src="/img/scr-img/app-3.png" alt="">
                    </div>
                    <div class="single-shot">
                        <img src="/img/scr-img/app-4.png" alt="">
                    </div>
                    <div class="single-shot">
                        <img src="/img/scr-img/app-5.png" alt="">
                    </div>
                    <div class="single-shot">
                        <img src="/img/scr-img/app-6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** App Screenshots Area End *****====== -->
<!-- ***** Client Feedback Area Start ***** -->
<section class="clients-feedback-area section_padding_50 clearfix" id="comentarios">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <!-- Heading Text  -->
                <div class="section-heading">
                    <h2>¿Qué dice la gente?</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="slider slider-for">
                    <!-- Client Feedback Text  -->
                    <div class="client-feedback-text text-center">
                        <div class="client">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="client-description text-center">
                            <p>
                                “Fue muy útil cuando al ingresar al municipio pude encontrar donde comprar lo
                                que necesitaba.”
                            </p>
                        </div>
                        <div class="star-icon text-center">
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                        </div>
                        <div class="client-name text-center">
                            <h5>Karen Izquierdo</h5>
                            <p>Turista</p>
                        </div>
                    </div>
                    <!-- Client Feedback Text  -->
                    <div class="client-feedback-text text-center">
                        <div class="client">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="client-description text-center">
                            <p>
                                “He encontrado lugares espectaculares en la ciudad gracias a la geoubicación de
                                la aplicación. Una muy buena guía para turistas.”
                            </p>
                        </div>
                        <div class="star-icon text-center">
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                        </div>
                        <div class="client-name text-center">
                            <h5>Luisa Borges</h5>
                            <p>Turista</p>
                        </div>
                    </div>
                    <!-- Client Feedback Text  -->
                    <div class="client-feedback-text text-center">
                        <div class="client">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="client-description text-center">
                            <p>
                                “Pude publicar las promociones que ofrezco en mi negocio con mucha facilidad.
                                Esto incrementó mis ventas ayudandome a llegar a mis clientes de manera
                                digital.”
                            </p>
                        </div>
                        <div class="star-icon text-center">
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                        </div>
                        <div class="client-name text-center">
                            <h5>Julio Dominguez</h5>
                            <p>Comerciante</p>
                        </div>
                    </div>
                    <!-- Client Feedback Text  -->
                    <div class="client-feedback-text text-center">
                        <div class="client">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div>
                        <div class="client-description text-center">
                            <p>
                                “Una excelente alternativa para comerciantes, empresarios y habitantes de la
                                ciudad. Es importante empezar a generar un comercio digital competitivo que
                                busque mejorar la calidad de vida de las personas.”
                            </p>
                        </div>
                        <div class="star-icon text-center">
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                            <i class="ion-ios-star"></i>
                        </div>
                        <div class="client-name text-center">
                            <h5>Ernesto Guzmán</h5>
                            <p>Residente del municipio</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Client Thumbnail Area -->
            <div class="col-12 col-md-6 col-lg-5">
                <div class="slider slider-nav">
                    <div class="client-thumbnail">
                        <img src="/img/bg-img/client-3.jpg" alt="">
                    </div>
                    <div class="client-thumbnail">
                        <img src="/img/bg-img/client-2.jpg" alt="">
                    </div>
                    <div class="client-thumbnail">
                        <img src="/img/bg-img/client-1.jpg" alt="">
                    </div>
                    <div class="client-thumbnail">
                        <img src="/img/bg-img/client-2.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Client Feedback Area End ***** -->
<!-- ***** Contact Us Area Start ***** -->
<section class="footer-contact-area section_padding_50 clearfix" id="contacto">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- Heading Text  -->
                <div class="section-heading">
                    <h2>¡Mantente en contacto con nosotros!</h2>
                    <div class="line-shape"></div>
                </div>
                <div class="footer-text">
                    <p>
                        Cualquier información, duda o comentario no dudes en comunicarte con
                        nosotros. Secretaría TIC, Alcaldía de Acacías.<br/>
                        <b>Acacías... <i>para que te quedes!</i></b>
                    </p>
                </div>
                <div class="address-text">
                    <p>
                        <span>Dirección: </span>
                        Cra 14 No. 13-30, Centro
                    </p>
                </div>
                <div class="phone-text">
                    <p>
                        <span>Teléfono: </span>
                        &nbsp;(8) 657 4632
                    </p>
                </div>
                <div class="email-text">
                    <p>
                        <span>Correo: </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tic@acacias-meta.gov.co
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <!-- Form Start-->
                <div class="contact_from">
                    <form action="#" method="post">
                        <!-- Message Input Area Start -->
                        <div class="contact_input_area">
                            <div class="row">
                                <!-- Single Input Area Start -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Tu nombre" required>
                                    </div>
                                </div>
                                <!-- Single Input Area Start -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Tu correo" required>
                                    </div>
                                </div>
                                <!-- Single Input Area Start -->
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Tu mensaje" required></textarea>
                                    </div>
                                </div>
                                <!-- Single Input Area Start -->
                                <div class="col-12">
                                    <button type="submit" class="btn submit-btn">Enviar</button>
                                </div>
                            </div>
                        </div>
                        <!-- Message Input Area End -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Contact Us Area End ***** -->
<!-- ***** Footer Area Start ***** -->
<footer class="footer-social-icon text-center section_padding_100_0 clearfix">
    <!-- footer logo -->
    <div class="footer-text">
        <h2>Alcaldía de Acacías</h2>
        <img src="http://www.acacias-meta.gov.co/apc-aa-files/30666133656436306239646334326334/logo-alcaldia-solo.png"/>
    </div>
    <!-- social icon-->
    <div class="footer-social-icon">
        <a href="http://www.acacias-meta.gov.co"><i class="fa fa-globe" aria-hidden="true"></i></a>
        <a href="https://www.facebook.com/AlcaldiaAcaciasMeta"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://twitter.com/alcaldiaAcacias"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/user/AlcaldiaAcacias"><i class="fa fa-youtube" aria-hidden="true"></i></a>
    </div>
    <div class="footer-menu">
        <nav>
            <ul>
                <li><a href="http://www.acacias-meta.gov.co">Acerca de...</a></li>
                <li><a href="http://www.acacias-meta.gov.co/condicionesUso.shtml">Condiciones de uso</a></li>
                <li><a href="http://186.147.247.55/ciudadano-pqrd/forms/frmradicadocrear.aspx">PQRS</a></li>
                <li><a href="mailto:contactenos@acacias-meta.gov.co?subject=Acacías%20para%20que%20te%20quedes">Contacto</a></li>
            </ul>
        </nav>
    </div>
    <!-- Foooter Text-->
    <div class="copyright-text">
        <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
        <p>Copyright ©2018 <a href="http://www.lacharme.com.co/" target="_blank">LaCharme Solution Universelle</a></p>

    </div>
</footer>
<!-- ***** Footer Area Start ***** -->
<!-- Jquery-2.2.4 JS -->
<script src="/js/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="/js/popper.min.js"></script>
<!-- Bootstrap-4 Beta JS -->
<script src="/js/bootstrap.min.js"></script>
<!-- All Plugins JS -->
<script src="/js/plugins.js"></script>
<!-- Slick Slider Js-->
<script src="/js/slick.min.js"></script>
<!-- Footer Reveal JS -->
<script src="/js/footer-reveal.min.js"></script>
<!-- Active JS -->
<script src="/js/active.js"></script>
</body>
</html>
