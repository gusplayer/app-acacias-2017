@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Mi establecimiento</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">{{$establecimiento->titulo}}</h3>

                                                <a href="/mi-establecimiento/{!! $establecimiento->id !!}/edit"
                                                   class="btn btn-info">Editar</a>

                                                <br>
                                                <br>
                                                <br>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="example1" class="table table-bordered table-striped">

                                                    <tbody>
                                                    @if($establecimiento)
                                                        <tr>
                                                            <td><b>Imágen</b></td>
                                                            <td><img class="img-responsive img" src="/img_lugar/{!! $establecimiento->imagen_principal !!}"> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Nombre</b></td>
                                                            <td>{{$establecimiento->titulo}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Nit</b></td>
                                                            <td>{!! $establecimiento->nit !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Dirección</b></td>
                                                            <td>{!! $establecimiento->direccion !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Teléfono</b></td>
                                                            <td>{!! $establecimiento->telefono !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Categoría</b></td>
                                                            <td>{!! $establecimiento->categoria->nombre !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Página web</b></td>
                                                            <td>{!! $establecimiento->web !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Descripcion</b></td>
                                                            <td>{!! html_entity_decode(substr($establecimiento->descripcion,0,240))!!}
                                                                ...
                                                            </td>
                                                        </tr>
                                                    @endif

                                                    </tbody>

                                                </table>

                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection