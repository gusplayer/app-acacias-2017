@extends('layouts.app')

@section('content')


    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Mis eventos creados</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <a href="/mi-establecimiento/eventos/create" type="button" class="btn btn-primary btn-sm">
                                            Crear nuevo evento
                                        </a>

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Eventos creados</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Titulo</th>
                                                        <th>Imágen</th>
                                                        <th>Contenido</th>
                                                        <th>Estado</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($eventos)
                                                        @foreach ($eventos as $evento)
                                                            <tr>
                                                                <td>{{$evento->titulo}}</td>
                                                                <td><img style="width: 100px; height: 80px;" class="img img-responsive" src="/img_eventos/{!! $evento->imagen_principal !!}"> </td>
                                                                <td>{!!  html_entity_decode(substr($evento->descripcion, 0, 30)) !!}...</td>
                                                                @if($evento->estado == 0)
                                                                    <td><span class="label label-info">Sin aprobar</span></td>
                                                                @else
                                                                    <td><span class="label label-success">Aprobado</span></td>
                                                                @endif
                                                                <td>
                                                                    <a href="/mi-establecimiento/eventos/{!! $evento->id !!}/edit"
                                                                       class="btn btn-info">Editar</a>

                                                                    <a style="text-decoration: none;"
                                                                       data-toggle="modal" href="#deleteModal"
                                                                       class="push1 btn btn-danger"
                                                                       id="{!! $evento->id !!}">Eliminar</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Titulo</th>
                                                        <th>Imágen</th>
                                                        <th>Contenido</th>
                                                        <th>Estado</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>

                                                {!! $eventos->render() !!}
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>


                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Eliminar evento</h4>
                                            </div>
                                            <form action="/mi-establecimiento/eliminar/eventos" method="POST" enctype="multipart/form-data">

                                                {!! csrf_field() !!}
                                                <div class="modal-body">
                                                    <p>Esta seguro que desea eliminar este evento?</p>

                                                    <input id="id-banners" type="hidden" name="id" required
                                                           class="form form-control">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        Cerrar
                                                    </button>
                                                    <input type="submit" class="btn btn-success" value="Eliminar">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $('.push1').click(function () {
                var essay_id = $(this).attr('id');
                $('#id-banners').val(essay_id);

                <?php
                    $var_example_php = "<script>document.write(essay_id);</script>";
                ?>
            });
        });
    </script>

@endsection
