@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">
                        <form action="/noticias" method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="col-md-8">
                                <label>Titulo</label>
                                <input id="titulo" name="titulo" value="{!! old('titulo') !!}" type="text"
                                       class="form form-control" required>
                            </div>

                            <div class="col-md-12">
                                <label>Descripción</label>
                                <textarea name="descripcion">{!! old('descripcion')  !!}</textarea>
                            </div>

                            <div class="col-md-8">
                                <label>Imágen</label>
                                <input type="file" name="imagen" class="form form-control" required>
                            </div>

                            <div class="col-md-8">
                                <label>Categoria</label>
                                <select name="categoria" class="form form-control" required>
                                    @foreach($categories as $categoria)
                                        <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                                    @endforeach()
                                </select>
                            </div>

                            <div class="col-md-8">
                                <label>Url Youtube</label>
                                <input type="url" name="youtube" value="{!! old('youtube') !!}"
                                       class="form form-control">
                            </div>

                            <br>
                            <br>
                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Guardar Noticia">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        CKEDITOR.replace( 'descripcion' );
    </script>

@endsection