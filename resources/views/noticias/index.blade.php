@extends('layouts.app')

@section('content')


    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Noticias creadas</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <a href="/noticias/create" type="button" class="btn btn-primary btn-sm">
                                            Crear nueva noticia
                                        </a>

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Noticias creadas</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Titulo</th>
                                                        <th>Contenido</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($noticias)
                                                        @foreach ($noticias as $noticia)
                                                            <tr>
                                                                <td>{{$noticia->titulo}}</td>
                                                                <td>{!!  html_entity_decode(substr($noticia->descripcion, 0, 30)) !!}...</td>
                                                                <td>
                                                                    <a href="/noticias/{!! $noticia->id !!}/edit"
                                                                       class="btn btn-info">Editar</a>

                                                                    <a style="text-decoration: none;"
                                                                       data-toggle="modal" href="#deleteModal"
                                                                       class="push1 btn btn-danger"
                                                                       id="{!! $noticia->id !!}">Eliminar</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Titulo</th>
                                                        <th>Contenido</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>

                                                {!! $noticias->render() !!}
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>


                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Eliminar Noticia</h4>
                                            </div>
                                            <form action="/eliminar/noticia" method="POST" enctype="multipart/form-data">

                                                {!! csrf_field() !!}
                                                <div class="modal-body">
                                                    <p>Esta seguro que desea eliminar esta noticia ?</p>

                                                    <input id="id-banners" type="hidden" name="id" required
                                                           class="form form-control">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        Cerrar
                                                    </button>
                                                    <input type="submit" class="btn btn-success" value="Eliminar">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $('.push1').click(function () {
                var essay_id = $(this).attr('id');
                $('#id-banners').val(essay_id);

                <?php
                    $var_example_php = "<script>document.write(essay_id);</script>";
                ?>
            });
        });
    </script>

@endsection
