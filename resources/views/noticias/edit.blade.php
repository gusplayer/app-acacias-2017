@extends('layouts.app')

@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active">

                        <label>Imágen noticia</label>
                        <br>
                        <div class="col-md-6 col-md-offset-3">
                            <img class="img img-responsive" src="/img_noticias/{!! $noticia->imagen_principal !!}">
                        </div>
                        <form action="/noticias/{!! $noticia->id !!}" method="POST" enctype="multipart/form-data">

                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="col-md-8">
                                <label>Titulo</label>
                                <input id="titulo" name="titulo" value="{!! $noticia->titulo !!}" type="text"
                                       class="form form-control" required>
                            </div>

                            <div class="col-md-12">
                                <label>Descripción</label>
                                <textarea name="descripcion">{!! $noticia->descripcion !!}</textarea>
                            </div>

                            <div class="col-md-8">
                                <label>Imágen</label>
                                <input type="file" name="imagen" class="form form-control">
                            </div>

                            <div class="col-md-8">
                                <label>Categoria</label>
                                <select name="categoria" class="form form-control" required>
                                    @foreach($categories as $categoria)
                                        @if($categoria->id == $noticia->categoria_noticia_id)
                                            <option value="{!! $categoria->id !!}"
                                                    selected>{!! $categoria->nombre !!}</option>
                                        @else
                                            <option value="{!! $categoria->id !!}"
                                                    selected>{!! $categoria->nombre !!}</option>
                                        @endif
                                    @endforeach()
                                </select>
                            </div>

                            <div class="col-md-8">
                                <label>Url Youtube</label>
                                <input type="url" name="youtube" value="{!! old('youtube') !!}"
                                       class="form form-control">
                            </div>

                            <br>
                            <br>
                            <div class="col-md-6" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-success" value="Editar noticia">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        CKEDITOR.replace( 'descripcion' );
    </script>


@endsection