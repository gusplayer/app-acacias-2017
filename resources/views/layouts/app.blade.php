<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/img/acacias_pqtq.ico">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Acacaias me gusta') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/plugins/ckeditor/ckeditor.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-93697026-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                @role('usuario')
                <a class="navbar-brand" href="/mi-establecimiento">
                    {{ config('app.nombre', 'ACACIAS ME GUSTA') }}
                </a>
                @endrole

                @role('administrador')
                <a class="navbar-brand" href="/establecimientos">
                    {{ config('app.nombre', 'ACACIAS ME GUSTA') }}
                </a>
                @endrole
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                    @else

                        @role('usuario')
                        <li><a href="{{ url('/mi-establecimiento') }}">Mi Establecimiento</a></li>
                        <li><a href="{{ url('/establecimiento/promociones') }}">Mis Promociones</a></li>
                        <li><a href="{{ url('/mi-establecimiento/eventos') }}">Mis Eventos</a></li>
                        @endrole

                        @role('administrador')
                        <li><a href="{{ url('/establecimientos') }}">Establecimientos</a></li>
                        <li><a href="{{ url('/lugares') }}">Lugares</a></li>
                        <li><a href="{{ url('/noticias') }}">Noticias</a></li>
                        <li><a href="{{ url('/eventos') }}">Eventos</a></li>
                        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                Categorias<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/categoria-lugares">Lugares</a>
                                    <a href="/categoria-eventos">Eventos</a>
                                    <a href="/categoria-noticias">Noticias</a>
                                    <a href="/categoria-promociones">Promociones</a>
                                </li>
                            </ul>
                        </li>

                        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                Subcategorias<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/subcategoria-lugares">Lugares</a>
                                    <a href="/subcategoria-eventos">Eventos</a>
                                    <a href="/subcategoria-noticias">Noticias</a>
                                    <a href="/subcategoria-promociones">Promociones</a>
                                </li>
                            </ul>
                        </li>
                        @endrole
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>
<!-- Scripts -->
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="/plugins/ckeditor/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "order": [[3, "DESC"]]
        });
    });
</script>
</body>
</html>
