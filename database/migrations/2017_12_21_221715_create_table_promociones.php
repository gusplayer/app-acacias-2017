<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePromociones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promociones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('imagen_principal');
            $table->string('direccion')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('web')->nullable();

            $table->integer('categoria_promociones_id')->unsigned();
            $table->foreign('categoria_promociones_id')->references('id')->on('categoria_promociones')->onDelete('CASCADE');

            $table->integer('lugares_id')->unsigned();
            $table->foreign('lugares_id')->references('id')->on('lugares')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promociones');
    }
}
