<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLugares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('direccion')->nullable();
            $table->string('imagen_principal');
            $table->text('descripcion')->nullable();
            $table->string('youtube')->nullable();
            $table->string('web')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->boolean('estado');

            $table->integer('categoria_lugares_id')->unsigned();
            $table->foreign('categoria_lugares_id')->references('id')->on('categoria_lugares')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lugares');
    }
}
