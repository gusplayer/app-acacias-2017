<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('imagen_principal');
            $table->string('lugar')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();


            $table->integer('categoria_eventos_id')->unsigned();
            $table->foreign('categoria_eventos_id')->references('id')->on('categoria_eventos')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventos');
    }
}
