<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImagenesLugares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes_lugares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagen');

            $table->integer('lugares_id')->unsigned();
            $table->foreign('lugares_id')->references('id')->on('lugares')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imagenes_lugares');
    }
}
