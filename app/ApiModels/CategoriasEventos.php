<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class CategoriasEventos extends Model
{
    protected $table = 'categoria_eventos';

    public function eventos()
    {
        return $this->hasMany('App\ApiModels\Eventos', 'categoria_eventos_id');
    }

    public function sub_categoria_eventos()
    {
        return $this->hasMany('App\ApiModels\SubcategoriasEventos', 'id_categoria');
    }
}
