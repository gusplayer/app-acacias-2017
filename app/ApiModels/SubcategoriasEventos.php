<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class SubcategoriasEventos extends Model
{
    protected $table = 'sub_categoria_eventos';

    public function categoria_eventos()
    {
        return $this->belongsTo('App\ApiModels\CategoriasEventos', 'id_categoria');
    }
}