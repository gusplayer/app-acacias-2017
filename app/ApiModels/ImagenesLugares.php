<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class ImagenesLugares extends Model
{
    protected $table = 'imagenes_lugares';

    public function lugares()
    {
        return $this->belongsTo('App\ApiModels\Lugares', 'lugares_id');
    }
}
