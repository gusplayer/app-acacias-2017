<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class SubcategoriasPromociones extends Model
{
    protected $table = 'sub_categoria_promociones';

    public function categoria_promociones()
    {
        return $this->belongsTo('App\ApiModels\CategoriasPromociones', 'id_categoria');
    }
}