<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class CategoriasPromociones extends Model
{
    protected $table = 'categoria_promociones';

    public function promociones()
    {
        return $this->hasMany('App\ApiModels\Promociones', 'categoria_promociones_id')->with('lugar');
    }

    public function sub_categoria_promociones()
    {
        return $this->hasMany('App\ApiModels\SubcategoriasPromociones', 'id_categoria');
    }
}
