<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class SubcategoriasLugares extends Model
{
    protected $table = 'sub_categoria_lugares';

     public function lugares()
    {
        return $this->hasMany('App\ApiModels\Lugares', 'sub_categoria_lugares_id');
    }

    public function categoria_lugares()
    {
        return $this->belongsTo('App\ApiModels\CategoriasLugares', 'id_categoria');
    }
}