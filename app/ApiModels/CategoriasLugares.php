<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class CategoriasLugares extends Model
{
    protected $table = 'categoria_lugares';

    public function lugares()
    {
        return $this->hasMany('App\ApiModels\Lugares', 'categoria_lugares_id');
    }

    public function sub_categoria_lugares()
    {
        return $this->hasMany('App\ApiModels\SubcategoriasLugares', 'id_categoria');
    }
}
