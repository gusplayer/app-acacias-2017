<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class Promociones extends Model
{
    protected $table = 'promociones';

    public function categoria_promociones()
    {
        return $this->belongsTo('App\ApiModels\CategoriasPromociones', 'categoria_promociones_id');
    }

    public function lugar()
    {
        return $this->belongsTo('App\ApiModels\Lugares', 'lugares_id');
    }
}
