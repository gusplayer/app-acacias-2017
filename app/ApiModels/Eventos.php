<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
    protected $table = 'eventos';

    public function categoria_eventos()
    {
        return $this->belongsTo('App\ApiModels\CategoriasEventos', 'categoria_eventos_id');
    }
}
