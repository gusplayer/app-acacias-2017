<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class Lugares extends Model
{
    protected $table = 'lugares';

    public function categoria()
    {
        return $this->belongsTo('App\ApiModels\CategoriasLugares', 'categoria_lugares_id');
    }

    public function sub_categoria()
    {
        return $this->belongsTo('App\ApiModels\SubcategoriasLugares', 'sub_categoria_lugares_id');
    }

    public function imagenes()
    {
        return $this->hasMany('App\ApiModels\ImagenesLugares', 'lugares_id');
    }

    public function promociones()
    {
        return $this->belongsTo('App\ApiModels\Promociones', 'lugares_id');
    }
}
