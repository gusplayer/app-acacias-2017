<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $table = 'noticias';

    public function categoria()
    {
        return $this->belongsTo('App\ApiModels\CategoriasNoticias', 'categoria_noticia_id');
    }
}
