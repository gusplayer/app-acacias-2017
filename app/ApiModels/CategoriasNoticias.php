<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class CategoriasNoticias extends Model
{
    protected $table = 'categoria_noticia';

    public function noticias()
    {
        return $this->hasMany('App\ApiModels\Noticias', 'categoria_noticia_id');
    }

    public function sub_categoria_noticias()
    {
        return $this->hasMany('App\ApiModels\SubcategoriasNoticias', 'id_categoria');
    }
}
