<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;

class SubcategoriasNoticias extends Model
{
    protected $table = 'sub_categoria_noticia';

    public function categoria_noticias()
    {
        return $this->belongsTo('App\ApiModels\CategoriasNoticias', 'id_categoria');
    }
}