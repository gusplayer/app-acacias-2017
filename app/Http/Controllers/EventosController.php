<?php

namespace App\Http\Controllers;

use App\ApiModels\Eventos;
use Illuminate\Http\Request;
use App\ApiModels\CategoriasEventos;
use Intervention\Image\Facades\Image;

class EventosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $eventos = Eventos::orderBy('id', 'DESC')->paginate(10);

        return view('eventos.index')->with(['eventos' => $eventos]);
    }

    public function create()
    {
        $categories = CategoriasEventos::get();

        return view('eventos.create')->with(['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'imagen' => 'required|file|image',
            'categoria' => 'required'
        ]);

        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $name_modified = $random = str_random(3) . date('is') . $filename;

        $evento = new Eventos();
        $evento->titulo = $request->titulo;
        $evento->lugar = $request->lugar;
        $evento->descripcion = $request->descripcion;

        if (Image::make($file->getRealPath())->save('img_eventos/' . $name_modified)) {
            $evento->imagen_principal = $name_modified;
        }

        $evento->categoria_eventos_id = $request->categoria;
        $evento->video = $request->video;
        $evento->latitud = $request->lat;
        $evento->longitud = $request->lng;
        $evento->save();

        return redirect()->back()->with(['success' => 'Evento creado con exito']);

    }

    public function edit($id)
    {
        $evento = Eventos::find($id);
        $categories = CategoriasEventos::get();

        return view('eventos.edit')->with(['evento' => $evento, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
        $evento = Eventos::where('id', $id)->first();

        $evento->titulo = $request->titulo;
        $evento->descripcion = $request->descripcion;
        $evento->categoria_eventos_id = $request->categoria;
        $evento->video = $request->video;
        $evento->latitud = $request->lat;
        $evento->longitud = $request->lng;
        $evento->lugar = $request->lugar;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500',
                '300')->save('img_eventos/' . $name_modified)
            ) {

                if (file_exists('img_eventos/' . $evento->imagen_principal)) {
                    unlink('img_eventos/' . $evento->imagen_principal);
                }
                $evento->imagen_principal = $name_modified;
            }
        }

        $evento->save();


        return redirect('/eventos')->with(['success' => 'Evento editado con exito']);
    }

    public function destroy(Request $request)
    {
        $evento = Eventos::where('id', $request->id)->first();

        if ($evento->imagen_principal) {
            if (file_exists('img_eventos/' . $evento->imagen_principal)) {
                unlink('img_eventos/' . $evento->imagen_principal);
            }
        }

        Eventos::where('id', $request->id)->delete();

        return redirect('/eventos')->with(['success' => 'Evento eliminado con exito']);
    }

    public function cambiarEstado($id, $estado)
    {
        if($estado == 'desactivar'){
            Eventos::where('id', $id)->update([
                'estado' => 0
            ]);
        }else if($estado == 'activar'){
            Eventos::where('id', $id)->update([
                'estado' => 1
            ]);
        }

        return redirect('/eventos')->with(['success' => 'Estado del evento editado con exito']);
    }
}
