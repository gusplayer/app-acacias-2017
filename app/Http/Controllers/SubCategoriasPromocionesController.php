<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasPromociones;
use App\ApiModels\SubcategoriasPromociones;
use Illuminate\Http\Request;

class SubCategoriasPromocionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaPromociones = SubcategoriasPromociones::with('categoria_promociones')->get();

        return view('subcategorias.promociones.index', [
            'categorias' => $categoriaPromociones
        ]);
    }

    public function create()
    {
        $categorias = CategoriasPromociones::get();

        return view('subcategorias.promociones.create', ['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $categoria = new SubcategoriasPromociones();
        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Subcategoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = SubcategoriasPromociones::find($id);
        $principales = CategoriasPromociones::get();

        return view('subcategorias.promociones.edit')->with(['categoria' => $categoria, 'principales' => $principales]);
    }

    public function update(Request $request, $id)
    {
        $categoria = SubcategoriasPromociones::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/subcategoria-promociones')->with(['success' => 'Subcategoria editada con exito']);
    }
}
