<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasNoticias;
use Illuminate\Http\Request;

class CategoriaNoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = CategoriasNoticias::paginate(10);

        return view('categorias.noticias.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        return view('categorias.noticias.create');
    }

    public function store(Request $request)
    {
        $categoria = new CategoriasNoticias();
        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Categoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = CategoriasNoticias::find($id);

        return view('categorias.noticias.edit')->with(['categoria' => $categoria]);
    }

    public function update(Request $request, $id)
    {
        $categoria = CategoriasNoticias::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/categoria-noticias')->with(['success' => 'Categoria editada con exito']);
    }
}
