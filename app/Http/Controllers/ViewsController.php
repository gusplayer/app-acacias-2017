<?php

namespace App\Http\Controllers;

use App\ApiModels\Lugares;
use App\ApiModels\Promociones;
use Illuminate\Http\Request;


class ViewsController extends Controller
{
    public function inicio()
    {
        $cantidad_negocios = Lugares::where('categoria_lugares_id', 1)->count();
        $promociones = Promociones::count();

        return view('inicio', ['cantidad_negocios' => $cantidad_negocios, 'cantidad_promociones' => $promociones]);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function registrar()
    {
        return view('establecimientos.registrar');
    }

    public function register(Request $request)
    {
        return view('establecimientos.add', ['email' => $request->email]);
    }

    public function home()
    {
        return view('home');
    }
}
