<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasEventos;
use Illuminate\Http\Request;
use App\ApiModels\SubcategoriasEventos;

class SubCategoriasEventosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = SubcategoriasEventos::with('categoria_eventos')->get();

        return view('subcategorias.eventos.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        $categorias = CategoriasEventos::get();

        return view('subcategorias.eventos.create', ['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'=> 'required',
            'categoria' => 'required'
        ]);

        $categoria = new SubcategoriasEventos();
        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Subcategoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = SubcategoriasEventos::find($id);
        $principales = CategoriasEventos::get();

        return view('subcategorias.eventos.edit')->with(['categoria' => $categoria, 'principales' => $principales]);
    }

    public function update(Request $request, $id)
    {
        $categoria = SubcategoriasEventos::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/subcategoria-eventos')->with(['success' => 'Subcategoria editada con exito']);
    }
}
