<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasLugares;
use App\ApiModels\SubcategoriasLugares;
use Illuminate\Http\Request;

class SubCategoriasLugaresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = SubcategoriasLugares::orderBy('id', 'DESC')->with('categoria_lugares')->get();

        return view('subcategorias.lugares.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        $categorias = CategoriasLugares::get();

        return view('subcategorias.lugares.create', ['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $categoria = new SubcategoriasLugares();
        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Subcategoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = SubcategoriasLugares::find($id);
        $principales = CategoriasLugares::get();

        return view('subcategorias.lugares.edit')->with(['categoria' => $categoria, 'principales' => $principales]);
    }

    public function update(Request $request, $id)
    {
        $categoria = SubcategoriasLugares::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/subcategoria-lugares')->with(['success' => 'Subcategoria editada con exito']);
    }
}
