<?php

namespace App\Http\Controllers;

use App\ApiModels\Lugares;
use Illuminate\Http\Request;
use App\ApiModels\Promociones;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\ApiModels\CategoriasPromociones;

class PromocionesMiEstablecimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:usuario');
    }

    public function index()
    {
        $establecimiento = Lugares::where('user_id', Auth::user()->id)->first();
        $promociones = Promociones::where('lugares_id', $establecimiento->id)->orderBy('id', 'DESC')->paginate(10);

        return view('miestablecimiento.promociones.index')->with(['promociones' => $promociones]);
    }

    public function create()
    {
        $categoria_promociones = CategoriasPromociones::get();

        return view('miestablecimiento.promociones.create', ['categorias' => $categoria_promociones]);
    }

    public function store(Request $request)
    {
        $establecimiento = Lugares::where('user_id', Auth::user()->id)->first();

        $this->validate($request, [
            'titulo' => 'required',
            'imagen' => 'required|file|image',
            'categoria' => 'required'
        ]);

        $promocion = new Promociones();
        $promocion->titulo = $request->titulo;
        $promocion->categoria_promociones_id = $request->categoria;
        $promocion->descripcion = $request->descripcion;
        $promocion->direccion = $request->direccion;
        $promocion->web = $request->web;
        $promocion->latitud = $request->lat;
        $promocion->longitud = $request->lng;

        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $name_modified = str_random(3) . date('is') . $filename;
        $callback = function ($constraint) {
            $constraint->upsize();
        };

        Image::make($file->getRealPath())->widen(800, $callback)->heighten(800,
            $callback)->save('img_promociones/' . $name_modified, 85);

        $promocion->lugares_id = $establecimiento->id;
        $promocion->imagen_principal = $name_modified;
        $promocion->save();

        return redirect('establecimiento/promociones')->with('success', 'Promoción añadida correctamente');
    }

    public function edit($id)
    {
        $establecimiento = Lugares::where('user_id', Auth::user()->id)->first();
        $categoria_promociones = CategoriasPromociones::get();

        $promocion = Promociones::where('lugares_id', $establecimiento->id)
            ->where('id', $id)
            ->first();

        return view('miestablecimiento.promociones.edit')->with([
            'promocion' => $promocion,
            'categorias' => $categoria_promociones
        ]);
    }

    public function update(Request $request, $id)
    {
        $establecimiento = Lugares::where('user_id', Auth::user()->id)->first();

        $promocion = Promociones::where('id', $id)
            ->where('lugares_id', $establecimiento->id)
            ->first();

        $promocion->titulo = $request->titulo;
        $promocion->categoria_promociones_id = $request->categoria;
        $promocion->descripcion = $request->descripcion;
        $promocion->direccion = $request->direccion;
        $promocion->web = $request->web;
        $promocion->latitud = $request->lat;
        $promocion->longitud = $request->lng;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500',
                '300')->save('/img_promociones/' . $name_modified)
            ) {

                if (file_exists('/img_promociones/' . $promocion->imagen_principal)) {
                    unlink('/img_promociones/' . $promocion->imagen_principal);
                }
                $promocion->imagen_principal = $name_modified;
            }
        }

        $promocion->save();

        return redirect('/establecimiento/promociones/' . $id . '/edit')->with(['success' => 'Promoción editada con éxito']);
    }

    public function destroy(Request $request)
    {
        $promocion = Promociones::where('id', $request->id)->first();

        if ($promocion->imagen_principal) {
            if (file_exists('img_promociones/' . $promocion->imagen_principal)) {
                unlink('img_promociones/' . $promocion->imagen_principal);
            }
        }

        Promociones::where('id', $request->id)->delete();

        return redirect('/establecimiento/promociones')->with(['success' => 'Promoción eliminada con éxito']);
    }
}
