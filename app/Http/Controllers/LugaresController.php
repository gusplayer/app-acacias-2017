<?php

namespace App\Http\Controllers;

use App\ApiModels\Lugares;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ApiModels\CategoriasLugares;
use Intervention\Image\Facades\Image;

class LugaresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $establecimientos = Lugares::where('categoria_lugares_id', '!=', '1')
            ->orderBy('id', 'DESC')
            ->get();

        return view('lugares.index')->with(['establecimientos' => $establecimientos]);
    }

    public function create()
    {
        $categorias = CategoriasLugares::where('id', '!=', '1')->get();

        return view('lugares.create')->with(['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'imagen' => 'required|file|image',
            'categoria' => 'required'
        ]);

        $establecimiento = new Lugares();
        $establecimiento->nit = $request->nit;
        $establecimiento->titulo = $request->titulo;
        $establecimiento->direccion = $request->direccion;
        $establecimiento->agregador = $request->agregador;
        $establecimiento->descripcion = $request->descripcion;
        $establecimiento->web = $request->web;
        $establecimiento->categoria_lugares_id = $request->categoria;
        $establecimiento->estado = 1;
        $establecimiento->telefono = $request->telefono;
        $establecimiento->latitud = $request->lat;
        $establecimiento->longitud = $request->lng;
        $establecimiento->user_id = Auth::user()->id;
        $establecimiento->agregador = Auth::user()->id;

        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $name_modified = str_random(3) . date('is') . $filename;
        $callback = function ($constraint) {
            $constraint->upsize();
        };

        \Image::make($file->getRealPath())->widen(800, $callback)->heighten(800,
            $callback)->save('img_lugar/' . $name_modified, 85);

        $establecimiento->imagen_principal = $name_modified;
        $establecimiento->save();


        return redirect()->back()->with(['success' => 'Agregado con exito']);
    }

    public function edit($id)
    {
        $lugar = Lugares::where('id', $id)->first();
        $categorias = CategoriasLugares::where('id', '!=', '1')->get();

        return view('lugares.edit')->with(['establecimiento' => $lugar, 'categorias' => $categorias]);
    }

    public function update(Request $request, $id)
    {
        $lugar = Lugares::where('id', $id)->first();

        $lugar->nit = $request->nit;
        $lugar->titulo = $request->titulo;
        $lugar->direccion = $request->direccion;
        $lugar->descripcion = $request->descripcion;
        $lugar->web = $request->web;
        $lugar->categoria_lugares_id = $request->categoria;
        $lugar->telefono = $request->telefono;
        $lugar->latitud = $request->lat;
        $lugar->longitud = $request->lng;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500', '300')->save('img_lugar/' . $name_modified)) {
                if (file_exists('img_lugar/' . $lugar->imagen_principal)) {
                    unlink('img_lugar/' . $lugar->imagen_principal);
                }
                $lugar->imagen_principal = $name_modified;
            }
        }

        $lugar->save();


        return redirect('/lugares')->with(['success' => 'Lugar editado con exito']);
    }

    public function cambiarEstado($id, $estado)
    {
        if($estado == 'desactivar'){
            Lugares::where('id', $id)->update([
                'estado' => 0
            ]);
        }else if($estado == 'activar'){
            Lugares::where('id', $id)->update([
                'estado' => 1
            ]);
        }

        return redirect('/lugares')->with(['success' => 'Estado del lugar editado con exito']);
    }
}
