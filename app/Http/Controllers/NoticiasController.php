<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasNoticias;
use App\ApiModels\Noticias;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class NoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $noticias = Noticias::orderBy('id', 'DESC')->paginate(10);

        return view('noticias.index')->with(['noticias' => $noticias]);
    }

    public function create()
    {
        $categories = CategoriasNoticias::get();

        return view('noticias.create')->with(['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'descripcion' => 'required',
            'imagen' => 'required|file|image',
            'categoria' => 'required'
        ]);

        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $name_modified = $random = str_random(3) . date('is') . $filename;

        $noticia = new Noticias();
        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;
        $noticia->youtube = $request->youtube;
        $noticia->categoria_noticia_id = $request->categoria;

        if (Image::make($file->getRealPath())->save('img_noticias/' . $name_modified)) {
            $noticia->imagen_principal = $name_modified;
        }
        $noticia->save();

        return redirect()->back()->with(['success' => 'Noticia creada con exito']);
    }

    public function edit($id)
    {
        $noticia = Noticias::find($id);
        $categories = CategoriasNoticias::get();

        return view('noticias.edit')->with(['noticia' => $noticia, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
        $noticia = Noticias::where('id', $id)->first();

        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;
        $noticia->categoria_noticia_id = $request->categoria;
        $noticia->youtube = $request->youtube;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500',
                '300')->save('img_noticias/' . $name_modified)
            ) {

                if (file_exists('img_noticias/' . $noticia->imagen_principal)) {
                    unlink('img_noticias/' . $noticia->imagen_principal);
                }
                $noticia->imagen_principal = $name_modified;
            }
        }

        $noticia->save();


        return redirect('/noticias')->with(['success' => 'Noticia editada con exito']);
    }

    public function destroy(Request $request)
    {
        $noticia = Noticias::where('id', $request->id)->first();

        if ($noticia->imagen_principal) {
            if (file_exists('img_noticias/' . $noticia->imagen_principal)) {
                unlink('img_noticias/' . $noticia->imagen_principal);
            }
        }

        Noticias::where('id', $request->id)->delete();

        return redirect('/noticias')->with(['success' => 'Noticia eliminada con exito']);
    }
}
