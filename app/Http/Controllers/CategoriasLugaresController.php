<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasEventos;
use App\ApiModels\CategoriasLugares;
use App\ApiModels\CategoriasNoticias;
use App\ApiModels\CategoriasPromociones;
use Illuminate\Http\Request;

class CategoriasLugaresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = CategoriasLugares::orderBy('id', 'DESC')->paginate(10);

        return view('categorias.lugares.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        return view('categorias.lugares.create');
    }

    public function store(Request $request)
    {
        $categoria = new CategoriasLugares();
        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Categoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = CategoriasLugares::find($id);

        return view('categorias.lugares.edit')->with(['categoria' => $categoria]);
    }

    public function update(Request $request, $id)
    {
        $categoria = CategoriasLugares::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/categoria-lugares')->with(['success' => 'Categoria editada con exito']);
    }
}
