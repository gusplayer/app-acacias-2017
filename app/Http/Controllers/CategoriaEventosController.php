<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiModels\CategoriasEventos;

class CategoriaEventosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = CategoriasEventos::paginate(10);

        return view('categorias.eventos.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        return view('categorias.eventos.create');
    }

    public function store(Request $request)
    {
        $categoria = new CategoriasEventos();
        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Categoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = CategoriasEventos::find($id);

        return view('categorias.eventos.edit')->with(['categoria' => $categoria]);
    }

    public function update(Request $request, $id)
    {
        $categoria = CategoriasEventos::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/categoria-eventos')->with(['success' => 'Categoria editada con exito']);
    }
}
