<?php

namespace App\Http\Controllers;

use App\User;
use App\ApiModels\Lugares;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class EstablecimientoController extends Controller
{
    public function agregar(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'email' => 'unique:users,email|required',
            'titulo' => 'required',
            'imagen' => 'file|image|required',
            'nit' => 'required',
            'direccion' => 'required',
        ]);

        return DB::transaction(function () use ($request) {

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $user->assignRole('usuario');

            $establecimiento = new Lugares();
            $establecimiento->nit = $request->nit;
            $establecimiento->titulo = $request->titulo;
            $establecimiento->direccion = $request->direccion;
            $establecimiento->agregador = '';
            $establecimiento->descripcion = $request->descripcion;
            $establecimiento->web = $request->web;
            $establecimiento->categoria_lugares_id = 1;
            $establecimiento->estado = 1;
            $establecimiento->user_id = $user->id;
            $establecimiento->telefono = $request->telefono;
            $establecimiento->latitud = $request->lat;
            $establecimiento->longitud = $request->lng;

            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = str_random(3) . date('is') . $filename;
            $callback = function ($constraint) {
                $constraint->upsize();
            };

            \Image::make($file->getRealPath())->widen(800, $callback)->heighten(800,
                $callback)->save('img_lugar/' . $name_modified, 85);

            $establecimiento->imagen_principal = $name_modified;
            $establecimiento->save();

            Auth::login($user);


            return redirect('/mi-establecimiento');
        });

        return redirect('/');
    }

    /*
    public function asignarRoles()
    {
        $users = User::get();

        foreach ($users as $user){
            $user->assignRole('usuario');
        }
    }
    */

    /*
    public function ver()
    {
        $usuarios = User::get();

        foreach ($usuarios as $user) {
            $lugares = Lugares::where('user_id', $user->id)->first();

            if(!$lugares && $user->id != '0'){
                User::where('id', $user->id)->delete();
            }
        }
    }
    */
}
