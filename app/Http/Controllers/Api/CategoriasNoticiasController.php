<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApiModels\CategoriasNoticias;

class CategoriasNoticiasController extends Controller
{
    public function show($id)
    {
        $noticias = CategoriasNoticias::where('id', $id)->with('noticias')->get();

        return response()->json([
            'data' => $noticias,
            'estado' => 200,
            'mensaje' => 'Noticias por categoria'
        ], 200);
    }
}
