<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\CategoriasPromociones;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriasPromocionesController extends Controller
{
    public function show($id)
    {
        $categorias_eventos = CategoriasPromociones::where('id', $id)->with('promociones')->get();

        return response()->json([
            'data' => $categorias_eventos,
            'estado' => 200,
            'mensaje' => 'Categoria de promocion con sus respectivas promociones'
        ], 200);
    }
}
