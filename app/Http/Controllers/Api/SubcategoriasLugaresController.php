<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\SubcategoriasLugares;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubcategoriasLugaresController extends Controller
{
    public function show($id)
    {
        $sub_categorias_lugares = SubcategoriasLugares::where('id', $id)->with('lugares')->get();

        return response()->json([
            'data' => $sub_categorias_lugares,
            'estado' => 200,
            'mensaje' => 'Lugares por subcategoria'
        ], 200);
    }
}