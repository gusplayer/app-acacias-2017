<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\Noticias;
use App\ApiModels\CategoriasNoticias;
use App\Http\Controllers\Controller;

class NoticiasController extends Controller
{
    public function index()
    {
        $data = [];
        $data['categorias_noticias'] = CategoriasNoticias::with('sub_categoria_noticias')->get();
        $data['noticias'] = Noticias::with('categoria')
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Listado de categorias y ultimas 10 noticias creadas'
        ], 200);
    }

    public function show($id)
    {
        $noticia = Noticias::where('id', $id)->first();

        return response()->json([
            'data' => $noticia,
            'estado' => 200,
            'mensaje' => 'Noticia'
        ], 200);
    }
}
