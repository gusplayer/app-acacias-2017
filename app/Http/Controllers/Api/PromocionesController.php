<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\CategoriasPromociones;
use App\ApiModels\Promociones;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PromocionesController extends Controller
{
    public function index()
    {
        $data = [];
        $data['categorias_promociones'] = CategoriasPromociones::with('sub_categoria_promociones')->get();
        $data['promociones'] = Promociones::paginate(20);

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Listado de promociones y categorias de promociones'
        ], 200);
    }

    public function show($id)
    {
        $data['evento'] = Promociones::where('id', $id)->with('lugar')->first();

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Informacion de promocion con informacion de su lugar'
        ], 200);
    }
}
