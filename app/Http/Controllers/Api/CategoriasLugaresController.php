<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\CategoriasLugares;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriasLugaresController extends Controller
{
    public function show($id)
    {
        $categorias_lugares = CategoriasLugares::where('id', $id)->with('lugares')->get();

        return response()->json([
            'data' => $categorias_lugares,
            'estado' => 200,
            'mensaje' => 'Lugares por categoria'
        ], 200);
    }
}
