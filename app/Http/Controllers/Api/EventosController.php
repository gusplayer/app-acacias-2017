<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\CategoriasEventos;
use App\ApiModels\Eventos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventosController extends Controller
{
    public function index()
    {
        $data = [];
        $data['categorias_eventos'] = CategoriasEventos::with('sub_categoria_eventos')->get();
        $data['eventos'] = Eventos::paginate(20);

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Listado de eventos y categorias de eventos'
        ], 200);
    }

    public function show($id)
    {
        $data['evento'] = Eventos::where('id', $id)->first();

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Informacion de evento'
        ], 200);
    }
}
