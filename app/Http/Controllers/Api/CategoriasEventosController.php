<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\CategoriasEventos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriasEventosController extends Controller
{
    public function show($id)
    {
        $categorias_eventos = CategoriasEventos::where('id', $id)->with('eventos')->get();

        return response()->json([
            'data' => $categorias_eventos,
            'estado' => 200,
            'mensaje' => 'Categorias de eventos'
        ], 200);
    }
}
