<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiModels\CategoriasLugares;
use App\ApiModels\CategoriasNoticias;
use App\ApiModels\CategoriasEventos;
use App\ApiModels\CategoriasPromociones;
use DB;

class SearchController extends Controller
{
	public function index(Request $request)
    {
    	//Limito la busqueda
        $TAMANO_PAGINA = 10;
        $pagina = false;
        //examino la pagina a mostrar y el inicio del registro a mostrar
        if ($request->input('page')!=null)
            $pagina = $request->input('page');

        if (!$pagina) {
            $inicio = 0;
            $pagina = 1;
        }else {
            $inicio = ($pagina - 1) * $TAMANO_PAGINA;
        }


    	if ($request->input('q')!=null){
            $q = $request->input('q');
            $busquedas = [];
            $categorias = [];
            $data = [];
      
            if (!\Cache::has('categorias')) {
                //lugares
                $categorias['categorias_lugares'] = CategoriasLugares::get();
                //noticias
                //$categorias['categorias_noticias'] = CategoriasNoticias::get();
                //eventos
                //$categorias['categorias_eventos'] = CategoriasEventos::get();
                //promociones
                //$categorias['categorias_promociones'] = CategoriasPromociones::get();

                \Cache::put('categorias', $categorias, 2880);
            }else{
                $categorias = \Cache::get('categorias');
            }
            
            if (!\Cache::has($q)) {
            	//lugares
            	$busquedas['lugares'] = DB::select("SELECT *,l.id FROM `lugares` l LEFT JOIN `categoria_lugares` cl ON l.categoria_lugares_id = cl.id WHERE MATCH(l.titulo,l.direccion,l.descripcion) AGAINST ('".$q."') OR MATCH(cl.nombre) AGAINST ('".$q."')");
            	//noticias
            	//$busquedas['noticias'] =DB::select("SELECT *,n.id FROM `noticias` n LEFT JOIN `categoria_noticia` cn ON n.categoria_noticia_id = cn.id WHERE MATCH(n.titulo,n.descripcion) AGAINST ('".$q."') OR MATCH(cn.nombre) AGAINST ('".$q."')");
            	//eventos
            	//$busquedas['eventos'] = DB::select("SELECT *,e.id FROM `eventos` e LEFT JOIN `categoria_eventos` ce ON e.categoria_eventos_id = ce.id WHERE MATCH(e.titulo,e.descripcion) AGAINST ('".$q."') OR MATCH(ce.nombre) AGAINST ('".$q."')");
            	//promociones
            	//$busquedas['promociones'] = DB::select("SELECT *,p.id FROM `promociones` p LEFT JOIN `categoria_promociones` cp ON p.categoria_promociones_id = cp.id WHERE MATCH(p.titulo,p.direccion,p.descripcion) AGAINST ('".$q."') OR MATCH(cp.nombre) AGAINST ('".$q."')");

            	 \Cache::put($q, $busquedas, 500);
        	}else{
        		$busquedas = \Cache::get($q);
        	}

        	//lugares
        	$data['categorias_lugares'] = $categorias['categorias_lugares'];
        	$aux = $this->paginacion($busquedas['lugares'],$inicio,$TAMANO_PAGINA,"Lugares");
            $data['lugares'] = $aux['contenido'];
            $data['numero_paginas'] = $aux['total_paginas'];
            
            //noticias
            /*$data['categorias_noticias'] = $categorias['categorias_noticias'];
            $aux = $this->paginacion($busquedas['noticias'],$inicio,$TAMANO_PAGINA,"Noticias");
            $data['noticias'] = $aux['contenido'];

            //eventos
            $data['categorias_eventos'] = $categorias['categorias_eventos'];
            $aux = $this->paginacion($busquedas['eventos'],$inicio,$TAMANO_PAGINA,"Eventos");
            $data['eventos'] = $aux['contenido'];

            //promociones
            $data['categorias_promociones'] = $categorias['categorias_promociones'];
            $aux = $this->paginacion($busquedas['promociones'],$inicio,$TAMANO_PAGINA,"Promociones");
            $data['promociones'] = $aux['contenido'];*/

            $mensaje = 'Resultado de '.$q;

    	}else{
    		$data = "Error en la busqueda";
    		$mensaje = 'Error';
    	}

    	return response()->json([
            'data' =>  $data,
            'estado' => 200,
            'mensaje' => $mensaje
        ], 200);
    }

    protected function paginacion($busqueda,$inicio,$TAMANO_PAGINA,$tipo)
    {
    	$db = array();
        foreach ($busqueda as $key => $val) {
            $val->tipo = $tipo;
            $db[] = $val; 
        }
        $data['contenido'] = array_slice($db, $inicio, $TAMANO_PAGINA);
        //calculo el total de paginas
        $registros = count($db);
        $total_paginas = ceil($registros / $TAMANO_PAGINA);
        $data['total_paginas'] = $total_paginas;
        return $data;
    }
}

