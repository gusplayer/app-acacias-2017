<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\Lugares;
use Illuminate\Http\Request;
use App\ApiModels\CategoriasLugares;
use App\Http\Controllers\Controller;
use DB;

class LugaresController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        $data['categorias_lugares'] = CategoriasLugares::with('sub_categoria_lugares')->get();
        $auxD = [];
        $auxD = Lugares::where('estado', 1)->get();

        //Limito la busqueda
        $TAMANO_PAGINA = 20;
        $pagina = false;
        //examino la pagina a mostrar y el inicio del registro a mostrar
        if ($request->input('page')!=null)
            $pagina = $request->input('page');

        if (!$pagina) {
            $inicio = 0;
            $pagina = 1;
        }else {
            $inicio = ($pagina - 1) * $TAMANO_PAGINA;
        }

        if (($request->input('lat')!=null)&&($request->input('lng')!=null)) {

            //Datos principales
            $lat = $request->input('lat');//$lat =  3.9877489145488885;
            $lng = $request->input('lng');//$lng = -73.7601367534424;
            $distance = 1; // lugares que se encuentren en un radio de 1KM
            $box = $this->getBoundaries($lat, $lng, $distance);

            //consulta principal de lugares cercanos
            $lugares=DB::select('SELECT titulo,( 6371 * ACOS( 
                                                 COS( RADIANS(' . $lat . ') ) 
                                                 * COS(RADIANS( latitud ) ) 
                                                 * COS(RADIANS( longitud ) 
                                                 - RADIANS(' . $lng . ') ) 
                                                 + SIN( RADIANS(' . $lat . ') ) 
                                                 * SIN(RADIANS( latitud ) ) 
                                                )
                                   ) AS distance 
                         FROM lugares
                         WHERE (latitud BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
                         AND (longitud BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
                         HAVING distance < ' . $distance . '
                         ORDER BY distance ASC');
            //calculo el total de paginas cercanas
            $registros = count($lugares);
            $total_paginas_cercanas = ceil($registros / $TAMANO_PAGINA);
            //
        
            if($total_paginas_cercanas>=$pagina){
                $data['lugares'] = DB::select('SELECT *, ( 6371 * ACOS( 
                                                 COS( RADIANS(' . $lat . ') ) 
                                                 * COS(RADIANS( latitud ) ) 
                                                 * COS(RADIANS( longitud ) 
                                                 - RADIANS(' . $lng . ') ) 
                                                 + SIN( RADIANS(' . $lat . ') ) 
                                                 * SIN(RADIANS( latitud ) ) 
                                                )
                                   ) AS distance 
                         FROM lugares
                         WHERE (latitud BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
                         AND (longitud BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
                         HAVING distance < ' . $distance . '
                         ORDER BY distance ASC LIMIT '.$inicio.',' . $TAMANO_PAGINA);
            }
            
            if ($total_paginas_cercanas>0) {
                //filtro para datos repetidos 
                $titulos = array();
                foreach ($auxD as $key => $val) {
                    $titulos[$val['titulo']]['bandera'] = true;
                }
                foreach ($lugares as $key => $val) {
                    $titulos[$val->titulo]['bandera'] = false;
                }
                $db = array();
                foreach ($auxD as $key => $val) {
                    if ($titulos[$val['titulo']]['bandera']) {
                        $db[] = $val;
                    }   
                }
                //fin filtro para datos repetidos
            }else{
                $db = $auxD;
            }

            //Detectar cantidad de valores validos no repetidos
            $inicio = ($total_paginas_cercanas - 1) * $TAMANO_PAGINA;
            if($inicio<0)$inicio=0;
            $query = DB::select('SELECT ( 6371 * ACOS( 
                                         COS( RADIANS(' . $lat . ') ) 
                                         * COS(RADIANS( latitud ) ) 
                                         * COS(RADIANS( longitud ) 
                                         - RADIANS(' . $lng . ') ) 
                                         + SIN( RADIANS(' . $lat . ') ) 
                                         * SIN(RADIANS( latitud ) ) 
                                        )
                           ) AS distance 
                 FROM lugares
                 WHERE (latitud BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
                 AND (longitud BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
                 HAVING distance < ' . $distance . '
                 ORDER BY distance ASC LIMIT '.$inicio.',' . $TAMANO_PAGINA);
            $contador = count($query);
            if ($total_paginas_cercanas<$pagina) {
                 if($contador<$TAMANO_PAGINA){
                    $take = $TAMANO_PAGINA-$contador;

                    if ($take<$TAMANO_PAGINA) {
                        $titulos = array();

                        //Datos Validos
                        foreach ($db as $key => $val) {
                             $titulos[$val['titulo']]['bandera'] = true; 
                        }
                        //Datos Invalidos
                        $i=0;
                        foreach ($db as $key => $val) {
                            if ($i==$take) { 
                                break;
                            }
                            $i++;
                            $titulos[$val['titulo']]['bandera'] = false; // Eliminar datos 
                        }
                        $dbaux = array();
                        foreach ($db as $key => $val) {
                            if ($titulos[$val['titulo']]['bandera']) {
                                $dbaux[] = $val;
                            }   
                        }

                        $db = $dbaux; //valores validos no repetidos
                    }else{
                        $dbaux = array();
                        foreach ($db as $key => $val) {
                            $dbaux[] = $val; 
                        }

                        $db = $dbaux;
                    }

                 }
                $pagina_vec = ($pagina-$total_paginas_cercanas-1)*$TAMANO_PAGINA;
                $data['lugares'] = array_slice($db, $pagina_vec, $TAMANO_PAGINA);

            }

            //calculo el total de paginas no repetidas
            $registros = count($db)-($TAMANO_PAGINA-$contador);
            $total_paginas_norep = ceil($registros / $TAMANO_PAGINA);

            //calculo el total de paginas totales
            $total_paginas = $total_paginas_norep+$total_paginas_cercanas;
            $data['numero_paginas'] = $total_paginas;


            //Add datos faltantes
            if (isset($data['lugares'])&&($total_paginas_cercanas>=$pagina)) {
                $cont = count($data['lugares']);
                if($cont<$TAMANO_PAGINA){
                    $take = $TAMANO_PAGINA-$cont;

                    $i=0;
                    $list = array();
                    foreach ($db as $key => $val) {
                        if ($i==$take) { // solo 10 resultados
                            break;
                        }
                        $i++;
                        $list[] = $val;
                    }
                    
                    $data['lugares'] = array_merge($data['lugares'],$list);
                }
            }

        }else{

            $db = array();
            foreach ($auxD as $key => $val) {
                $db[] = $val; 
            }
            $data['lugares'] = array_slice($db, $inicio, $TAMANO_PAGINA);
            //calculo el total de paginas
            $registros = count($db);
            $total_paginas = ceil($registros / $TAMANO_PAGINA);
            $data['numero_paginas'] = $total_paginas;

        }
        
        

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Listado de lugares y categorias de lugares'
        ], 200);
    }

    public function show($id)
    {
        $data['lugares'] = Lugares::where('id', $id)->with('imagenes')->first();

        return response()->json([
            'data' => $data,
            'estado' => 200,
            'mensaje' => 'Listado de lugares'
        ], 200);
    }

    protected function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
         
        // Los angulos para cada direcciÃ³n
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
             $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                     'max_lat' => $return['north']['lat'],
                     'min_lng' => $return['west']['lng'],
                     'max_lng' => $return['east']['lng']);
    }

}