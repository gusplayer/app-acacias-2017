<?php

namespace App\Http\Controllers;

use App\ApiModels\CategoriasNoticias;
use App\ApiModels\SubcategoriasNoticias;
use Illuminate\Http\Request;

class SubCategoriasNoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaLugares = SubcategoriasNoticias::get();

        return view('subcategorias.noticias.index', [
            'categorias' => $categoriaLugares
        ]);
    }

    public function create()
    {
        $categorias = CategoriasNoticias::get();

        return view('subcategorias.noticias.create', ['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $categoria = new SubcategoriasNoticias();
        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Subcategoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = SubcategoriasNoticias::find($id);
        $principales = CategoriasNoticias::get();

        return view('subcategorias.noticias.edit')->with(['categoria' => $categoria, 'principales' => $principales]);
    }

    public function update(Request $request, $id)
    {
        $categoria = SubcategoriasNoticias::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->id_categoria = $request->categoria;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/subcategoria-noticias')->with(['success' => 'Subcategoria editada con exito']);
    }
}
