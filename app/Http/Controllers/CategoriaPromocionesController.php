<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiModels\CategoriasPromociones;

class CategoriaPromocionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $categoriaPromociones = CategoriasPromociones::paginate(10);

        return view('categorias.promociones.index', [
            'categorias' => $categoriaPromociones
        ]);
    }

    public function create()
    {
        return view('categorias.promociones.create');
    }

    public function store(Request $request)
    {
        $categoria = new CategoriasPromociones();
        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->nombre, '-');
        $categoria->save();

        return redirect()->back()->with(['success' => 'Categoria creada con exito']);
    }

    public function edit($id)
    {
        $categoria = CategoriasPromociones::find($id);

        return view('categorias.promociones.edit')->with(['categoria' => $categoria]);
    }

    public function update(Request $request, $id)
    {
        $categoria = CategoriasPromociones::where('id', $id)->first();

        $categoria->nombre = $request->nombre;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();

        return redirect('/categoria-promociones')->with(['success' => 'Categoria editada con exito']);
    }
}
