<?php

namespace App\Http\Controllers;

use App\ApiModels\Lugares;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ApiModels\CategoriasLugares;
use Intervention\Image\Facades\Image;

class MiEstablecimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:usuario');
    }

    public function index()
    {
        $establecimiento = Lugares::where('user_id', Auth::user()->id)
            ->with('categoria')
            ->first();

        return view('miestablecimiento.index')->with(['establecimiento' => $establecimiento]);
    }

    public function edit($id)
    {
        $lugar = Lugares::where('id', $id)->where('user_id', Auth::user()->id)->first();
        $categorias = CategoriasLugares::get();

        return view('miestablecimiento.edit')->with(['establecimiento' => $lugar, 'categorias' => $categorias]);
    }

    public function update(Request $request, $id)
    {
        $lugar = Lugares::where('id', $id)->first();

        $lugar->nit = $request->nit;
        $lugar->titulo = $request->titulo;
        $lugar->direccion = $request->direccion;
        $lugar->descripcion = $request->descripcion;
        $lugar->web = $request->web;
        $lugar->categoria_lugares_id = $request->categoria;
        $lugar->telefono = $request->telefono;
        $lugar->latitud = $request->lat;
        $lugar->longitud = $request->lng;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500',
                '300')->save('img_lugar/' . $name_modified)
            ) {

                if (file_exists('img_lugar/' . $lugar->imagen_principal)) {
                    unlink('img_lugar/' . $lugar->imagen_principal);
                }
                $lugar->imagen_principal = $name_modified;
            }
        }

        $lugar->save();


        return redirect('/mi-establecimiento')->with(['success' => 'Establecimiento editado con exito']);
    }
}
