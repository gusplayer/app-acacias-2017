<?php

namespace App\Http\Controllers;

use App\User;
use App\ApiModels\Lugares;
use App\ApiModels\Eventos;
use Illuminate\Http\Request;
use App\ApiModels\Promociones;
use App\ApiModels\ImagenesLugares;
use App\ApiModels\CategoriasLugares;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class EstablecimientosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:administrador');
    }

    public function index()
    {
        $establecimientos = Lugares::where('categoria_lugares_id', '1')
            ->orderBy('id', 'DESC')
            ->get();

        return view('establecimientos.index')->with(['establecimientos' => $establecimientos]);
    }

    public function create()
    {
        $categorias = CategoriasLugares::get();

        return view('establecimientos.create')->with(['categorias' => $categorias]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'imagen' => 'required|file|image',
            'categoria' => 'required'
        ]);

        $user = '';

        if ($request->name && $request->email && $request->password) {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
        }

        $establecimiento = new Lugares();
        $establecimiento->nit = $request->nit;
        $establecimiento->titulo = $request->titulo;
        $establecimiento->direccion = $request->direccion;
        $establecimiento->agregador = $request->agregador;
        $establecimiento->descripcion = $request->descripcion;
        $establecimiento->web = $request->web;
        $establecimiento->categoria_lugares_id = 1;
        $establecimiento->estado = 1;
        $establecimiento->telefono = $request->telefono;
        $establecimiento->latitud = $request->lat;
        $establecimiento->longitud = $request->lng;
        if ($user) {
            $establecimiento->user_id = $user->id;
            $establecimiento->agregador = Auth::user()->id;
        } else {
            $establecimiento->user_id = Auth::user()->id;
            $establecimiento->agregador = Auth::user()->id;
        }

        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $name_modified = str_random(3) . date('is') . $filename;
        $callback = function ($constraint) {
            $constraint->upsize();
        };

        \Image::make($file->getRealPath())->widen(800, $callback)->heighten(800,
            $callback)->save('img_lugar/' . $name_modified, 85);

        $establecimiento->imagen_principal = $name_modified;
        $establecimiento->save();


        return redirect()->back()->with(['success' => 'Agregado con exito']);
    }

    public function edit($id)
    {
        $lugar = Lugares::where('id', $id)->first();
        $categorias = CategoriasLugares::get();

        return view('establecimientos.edit')->with(['establecimiento' => $lugar, 'categorias' => $categorias]);
    }

    public function update(Request $request, $id)
    {
        $lugar = Lugares::where('id', $id)->first();
        $lugar->nit = $request->nit;
        $lugar->titulo = $request->titulo;
        $lugar->direccion = $request->direccion;
        $lugar->descripcion = $request->descripcion;
        $lugar->web = $request->web;
        $lugar->categoria_lugares_id = $request->categoria;
        $lugar->telefono = $request->telefono;
        $lugar->latitud = $request->lat;
        $lugar->longitud = $request->lng;

        if ($request->imagen) {
            $file = $request->imagen;
            $filename = $file->getClientOriginalName();
            $name_modified = $random = str_random(3) . date('is') . $filename;

            if (Image::make($file->getRealPath())->resize('500', '300')->save('img_lugar/' . $name_modified)) {
                if (file_exists('img_lugar/' . $lugar->imagen_principal)) {
                    unlink('img_lugar/' . $lugar->imagen_principal);
                }
                $lugar->imagen_principal = $name_modified;
            }
        }

        $lugar->save();


        return redirect('/establecimientos')->with(['success' => 'Lugar editado con exito']);
    }

    public function cambiarEstado($id, $estado)
    {
        if ($estado == 'desactivar') {
            Lugares::where('id', $id)->update([
                'estado' => 0
            ]);
        } else if ($estado == 'activar') {
            Lugares::where('id', $id)->update([
                'estado' => 1
            ]);
        }

        return redirect('/establecimientos')->with(['success' => 'Estado del establecimiento editado con exito']);
    }

    public function destroy(Request $request)
    {
        $lugar = Lugares::where('id', $request->id)->first();
        if (file_exists('/img_lugar/' . $lugar->imagen_principal)) {
            unlink('/img_lugar/' . $lugar->imagen_principal);
        }

        $imagenes_lugares  = ImagenesLugares::where('lugares_id', $request->id)->get();
        foreach ($imagenes_lugares as $imagen) {
            if (file_exists('/img_lugares/' . $imagen->imagen)) {
                unlink('/img_lugares/' . $imagen->imagen);
            }
        }

        $promociones = Promociones::where('lugares_id', $request->id)->get();
        foreach ($promociones as $promocion) {
            if (file_exists('/img_promociones/' . $promocion->imagen_principal)) {
                unlink('/img_promociones/' . $promocion->imagen_principal);
            }
        }
        $lugar->delete();

        return redirect('/establecimientos')->with(['success' => 'Establecimiento eliminado con éxito']);
    }
}
